import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
// @ts-ignore
export class SpinnerService {

   visibility = new BehaviorSubject(false);

  show() {
    this.visibility.next(true);
  }

  hide() {
    this.visibility.next(false);
  }
}
