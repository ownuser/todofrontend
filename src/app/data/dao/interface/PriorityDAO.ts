import { CommonDAO  } from './CommonDAO';
import { Priority } from '../../../model/Priority';
import { PrioritySearchValue  } from '../search/SearchObjects';
import { Observable } from 'rxjs';

export interface PriorityDAO extends CommonDAO<Priority>{
  findPriorities(prioritySearchValue: PrioritySearchValue): Observable<any>;
}
