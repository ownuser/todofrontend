import { CommonDAO } from './CommonDAO';
import { Task } from '../../../model/Task';
import { Observable } from 'rxjs';

import { TaskSearchValue } from '../search/SearchObjects';

export interface TaskDAO extends CommonDAO<Task>{

    findTasks(taskSearchValue: TaskSearchValue): Observable<any>;
}
