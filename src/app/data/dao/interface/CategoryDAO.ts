import { CommonDAO } from './CommonDAO';
import { Category } from '../../../model/Category';
import { Observable } from 'rxjs';
import { CategorySearchValue } from '../search/SearchObjects';

export interface CategoryDAO extends CommonDAO<Category>{
  findCategories(categorySearchValue: CategorySearchValue): Observable<any>;
}
