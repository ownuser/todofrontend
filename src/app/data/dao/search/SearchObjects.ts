
export class StatSearchValue {
  completed: number;
  uncompleted: number;
}

export class CategorySearchValue {
   title: string = null;
}

export class PrioritySearchValue {
  title: string = null;
}

export class TaskSearchValue {
  title = '';
  completed: number = null;
  priorityId: number = null;
  categoryId: number = null;

  pageNumber = 0;
  pageSize = 5;

  sortColumn = 'title';
  sortDirection = 'asc';
}
