import {Inject, Injectable, InjectionToken} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Task} from '../../../model/Task';
import {Observable} from 'rxjs';
import {TaskDAO} from '../interface/TaskDAO';
import {TaskSearchValue} from '../search/SearchObjects';
import {CommonService} from './CommonService';

export const TASK_URL_TOKEN = new InjectionToken<string>('url');

@Injectable({
  providedIn: 'root'
})
export class TaskService extends CommonService<Task> implements TaskDAO{

  constructor(@Inject(TASK_URL_TOKEN) private baseUrl, private http: HttpClient) {
    super(baseUrl, http);
  }

  findTasks(taskSearchValue: TaskSearchValue): Observable<any> {
    return this.http.post<Task[]>(`${this.baseUrl}/search`, taskSearchValue);
  }
}
