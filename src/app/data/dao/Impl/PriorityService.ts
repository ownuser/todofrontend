import {Inject, Injectable, InjectionToken} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PrioritySearchValue} from '../search/SearchObjects';
import {PriorityDAO} from '../interface/PriorityDAO';
import {Priority} from '../../../model/Priority';
import {CommonService} from './CommonService';

export const PRIORITY_URL_TOKEN = new InjectionToken<string>('url');

@Injectable({
  providedIn: 'root'
})
export class PriorityService extends CommonService<Priority> implements PriorityDAO {

  constructor(@Inject(PRIORITY_URL_TOKEN) private baseUrl, private http: HttpClient) {
    super(baseUrl, http);
  }

  findPriorities(prioritySearchValue: PrioritySearchValue) {
    return this.http.post<Priority[]>(`${this.baseUrl}/search`, prioritySearchValue);
  }
}
