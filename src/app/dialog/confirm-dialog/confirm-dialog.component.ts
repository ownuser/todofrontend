import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DialogAction, DialogResult} from "../../object/DialogResult";

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {
  message: string;
  dialogTitle: string;

  constructor(private dialogRef: MatDialogRef<ConfirmDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: {dialogTitle: string, message: string}) {
              this.message = data.message;
              this.dialogTitle = data.dialogTitle;
  }

  ngOnInit(): void {
  }

  confirm(): void {
    this.dialogRef.close(new DialogResult(DialogAction.OK));
  }

  cancel(): void {
    this.dialogRef.close(new DialogResult(DialogAction.CANCEL));
  }
}
