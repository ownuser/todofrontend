import {Component, Inject, OnInit} from '@angular/core';
import {Priority} from '../../model/Priority';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {DialogAction, DialogResult} from '../../object/DialogResult';

@Component({
  selector: 'app-edit-priority-dialog',
  templateUrl: './edit-priority-dialog.component.html',
  styleUrls: ['./edit-priority-dialog.component.css']
})
export class EditPriorityDialogComponent implements OnInit {

  dialogTitle: string;
  canDelete = false;
  priority: Priority;

  constructor(private dialogRef: MatDialogRef<EditPriorityDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: [Priority, string],
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.priority = this.data[0];
    this.dialogTitle = this.data[1];

    if (!this.priority && this.priority.id > 0) {
      this.canDelete = false;
    }
  }

  confirm() {
    this.dialogRef.close(new DialogResult(DialogAction.SAVE, this.priority));
  }

  cancel() {
    this.dialogRef.close(new DialogResult(DialogAction.CANCEL));
  }

  delete() {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dataTitle: 'Confirm your action',
        message: `Are you sour want you wont delete this priority: "${this.priority.title}?"`
      },
      autoFocus: false
    });

    dialog.afterClosed().subscribe(result => {

      if (!result) {
        return;
      }

      if (result.action === DialogAction.OK) {
        this.dialogRef.close(new DialogResult(DialogAction.DELETE));
      }
    });
  }

}
