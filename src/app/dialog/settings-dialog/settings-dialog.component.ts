import {Component, OnInit} from '@angular/core';
import {Priority} from '../../model/Priority';
import {MatDialogRef} from '@angular/material/dialog';
import {PriorityService} from '../../data/dao/Impl/PriorityService';
import {DialogAction, DialogResult} from '../../object/DialogResult';

@Component({
  selector: 'app-settings-dialog',
  templateUrl: './settings-dialog.component.html',
  styleUrls: ['./settings-dialog.component.css']
})
export class SettingsDialogComponent implements OnInit {

  priorities: Priority[];
  settingsChanged = false;

  constructor(private dialogRef: MatDialogRef<SettingsDialogComponent>, private priorityService: PriorityService) { }

  ngOnInit(): void {
    this.fillAllPriorities();
  }

  fillAllPriorities() {
    return this.priorityService.findAll().subscribe(result => this.priorities = result);
  }

  close() {
    if (this.settingsChanged) {
      this.dialogRef.close(new DialogResult(DialogAction.SETTINGS_CHANGED));
    }else{
      this.dialogRef.close(new DialogResult(DialogAction.CANCEL));
    }
  }

  addPriority(priority: Priority) {
    this.settingsChanged = true;
    this.priorityService.add(priority).subscribe(result => {
      this.priorities.push(result);
    });
  }

  updatePriority(priority: Priority) {
    this.settingsChanged = true;
    this.priorityService.update(priority).subscribe(() => {
       this.priorities[this.getPriorityIndex(priority)] = priority;
    });
  }

  deletePriority(priority: Priority) {
    this.settingsChanged = true;
    this.priorityService.delete(priority.id).subscribe(() => {
      this.priorities.splice(this.getPriorityIndex(priority), 1);
    });
  }

  getPriorityIndex(priority: Priority) {
    const tmpPriority = this.priorities.find(pr => pr.id === priority.id);
    return this.priorities.indexOf(tmpPriority);
  }
}
