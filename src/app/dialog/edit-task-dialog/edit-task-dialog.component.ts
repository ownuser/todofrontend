import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Task} from '../../model/Task';
import {Category} from '../../model/Category';
import {Priority} from '../../model/Priority';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {DialogAction, DialogResult} from '../../object/DialogResult';
import {DeviceDetectorService} from "ngx-device-detector";


@Component({
  selector: 'app-edit-task-dialog',
  templateUrl: './edit-task-dialog.component.html',
  styleUrls: ['./edit-task-dialog.component.css']
})
export class EditTaskDialogComponent implements OnInit {

  task: Task;
  categories: Category[];
  priorities: Priority[];

  canDelete: boolean;
  canComplete: boolean;
  dialogTitle: string;
  today = new Date();

  newTitle: string;
  newPriority: number;
  newCategory: number;
  oldCategoryId: number;
  newDate: Date;

  isMobile = this.deviceService.isMobile();

  constructor(private dialogRef: MatDialogRef<EditTaskDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: [Task, string, Category[], Priority[]],
              private deviceService: DeviceDetectorService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.task = this.data[0];
    this.dialogTitle = this.data[1];
    this.categories = this.data[2];
    this.priorities = this.data[3];

    this.newTitle = this.task.title;

    // for edit
    if (this.task && this.task.id > 0) {
      this.canDelete = true;
      this.canComplete = true;
    }

    this.newTitle = this.task.title;

    if (this.task.priority) {
      this.newPriority = this.task.priority.id;
    }

    if (this.task.category) {
      this.newCategory = this.task.category.id;
      this.oldCategoryId = this.task.category.id;
    }

    if (this.task.date) {
      this.newDate = new Date(this.task.date);
    }
  }

  confirm(): void {
    this.task.title = this.newTitle;
    this.task.category = this.findCategoryById(this.newCategory);
    this.task.priority = this.findPriorityById(this.newPriority);
    this.task.oldCategory = this.findCategoryById(this.oldCategoryId);
    this.task.date = (!this.newDate) ? null : this.newDate;

    this.dialogRef.close(new DialogResult(DialogAction.SAVE, this.task));
  }

  cancel(): void {
    this.dialogRef.close(new DialogResult(DialogAction.CANCEL));
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
          dataTitle: 'Confirm your action',
          message: `Are you sour want you wont delete this task: "${this.task.title}?"`
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      if (result.action === DialogAction.OK) {
         this.dialogRef.close(new DialogResult(DialogAction.DELETE));
      }
    });
  }

  activate(): void {
    this.dialogRef.close(new DialogResult(DialogAction.ACTIVE));
  }

  complete() {
    this.dialogRef.close(new DialogResult(DialogAction.COMPLETE));
  }

  private findPriorityById(tmpPriorityId: number) {
    return this.priorities.find(t => t.id === tmpPriorityId);
  }

  private findCategoryById(tmpCategoryId: number) {
    return this.categories.find(t => t.id === tmpCategoryId);
  }

  addDays(days: number) {
    this.newDate = new Date();
    this.newDate.setDate(this.today.getDate() + days);
  }

  setToday() {
    this.newDate = this.today;
  }
}
