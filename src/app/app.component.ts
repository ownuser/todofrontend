import {Component, OnInit} from '@angular/core';
import {Task} from './model/Task';
import {Category} from './model/Category';
import {Priority} from './model/Priority';
import {DeviceDetectorService} from 'ngx-device-detector';
import {CategoryService} from './data/dao/Impl/CategoryService';
import {CategorySearchValue, TaskSearchValue} from './data/dao/search/SearchObjects';
import {TaskService} from './data/dao/Impl/TaskService';
import {PageEvent} from '@angular/material/paginator';
import {PriorityService} from './data/dao/Impl/PriorityService';
import {Stat} from './model/Stat';
import {DashboardData} from './object/DashboardData';
import {StatService} from './data/dao/Impl/StatService';
import {MatDialog} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {SpinnerService} from '../service/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  tasks: Task[];
  categories: Category[];
  priorities: Priority[];

  stat: Stat;
  selectedCategory: Category = null;

  dash: DashboardData = new DashboardData();

  // Show / Hide statistic - Search
  toggleStatistic = true;
  showSearch = true;

  totalTasksFounded: number;
  spinner: SpinnerService;

  // menu
  menuOpened: boolean;
  menuMode: string;
  menuPosition: string;
  showBackdrop: boolean;

  // mobile version
  isMobile: boolean;
  isTablet: boolean;

  uncompletedCountForCategoryAll: number;
  categorySearchValue = new CategorySearchValue();
  taskSearchValue = new TaskSearchValue();

  constructor(private deviceService: DeviceDetectorService,
              private categoryService: CategoryService,
              private taskService: TaskService,
              private priorityService: PriorityService,
              private statService: StatService,
              private dialog: MatDialog,
              private spinnerService: SpinnerService) {
        this.isMobile = deviceService.isMobile();
        this.isTablet = deviceService.isTablet();
        this.spinner = spinnerService;
        this.setMenuDisplayParams();

        this.statService.getOverallStat().subscribe(result => {
          this.stat = result;
          this.uncompletedCountForCategoryAll = this.stat.uncompletedTotal;

          this.fillAllCategories().subscribe(res => {
            this.categories = res;

            this.selectCategory(this.selectedCategory);
          });
        });
  }

  ngOnInit() {
    this.fillAllPriorities();
  }

  // Categories
  fillAllCategories(): Observable<Category[]> {
    return this.categoryService.findAll();
  }

  addCategory(category: Category) {
    this.categoryService.add(category).subscribe(result => {
        this.searchCategory(this.categorySearchValue);
    });
  }

  updateCategory(category: Category) {
    this.categoryService.update(category).subscribe(result => {
      this.searchCategory(this.categorySearchValue);
      this.searchTasks(this.taskSearchValue);
    });
  }

  deleteCategory(category: Category) {
    this.categoryService.delete(category.id).subscribe(result => {
      this.selectedCategory = null; // all categories

      this.searchCategory(this.categorySearchValue);
      this.selectCategory(this.selectedCategory);
    });
  }

  selectCategory(category: Category) {
    if (category) {
      this.fillDashData(category.completedCount, category.uncompletedCount);
    }else{
      this.fillDashData(this.stat.completedTotal, this.stat.uncompletedTotal);
    }

    this.taskSearchValue.pageNumber = 0;
    this.selectedCategory = category;

    this.taskSearchValue.categoryId = category ? category.id : null;
    this.searchTasks(this.taskSearchValue);

    if (this.isMobile) {
      this.menuOpened = false;
    }
  }

  searchCategory(categorySearchValue: CategorySearchValue) {
    this.categoryService.findCategories(categorySearchValue).subscribe(result => {
      this.categories = result;
    });
  }

  // Priority
  fillAllPriorities() {
    this.priorityService.findAll().subscribe(result => {
      this.priorities = result;
    });
  }

  // Statistic
  fillDashData(completedCount: number, uncompletedCount: number) {
     this.dash.completedTotal = completedCount;
     this.dash.uncompletedTotal = uncompletedCount;
  }

  // Task
  addTask(task: Task) {
    this.taskService.add(task).subscribe(result => {

      if (task.category) {
        this.updateCategoryCounter(task.category);
      }
      this.updateOverallCounter();
      this.searchTasks(this.taskSearchValue);
    });
  }

  updateTask(task: Task) {

    this.taskService.update(task).subscribe(result => {
      if (task.oldCategory) {
        this.updateCategoryCounter(task.oldCategory);
      }

      if (task.category) {
        this.updateCategoryCounter(task.category);
      }

      this.updateOverallCounter();
      this.searchTasks(this.taskSearchValue);
    });
  }

  deleteTask(task: Task) {
    this.taskService.delete(task.id).subscribe(result => {
      if (task.category) {
        this.updateCategoryCounter(task.category);
      }

      this.updateOverallCounter();
      this.searchTasks(this.taskSearchValue);
    });
  }

  toggleStat(toggle: boolean) {
    this.toggleStatistic = toggle;
  }

  setMenuDisplayParams() {
    this.menuPosition = 'left';

    if (this.isMobile) {
      this.menuOpened = false;
      this.menuMode = 'over';
      this.showBackdrop = true;
    } else {
      this.menuOpened = true;
      this.menuMode = 'push';
      this.showBackdrop = false;
    }
  }

  onClosedMenu() {
    this.menuOpened = false;
  }

  toggleMenu() {
    this.menuOpened = !this.menuOpened;
  }

  searchTasks(taskSearchValue: TaskSearchValue) {
    this.taskSearchValue = taskSearchValue;

    this.taskService.findTasks(this.taskSearchValue).subscribe(result => {
        if (result.totalPages > 0 && this.taskSearchValue.pageNumber >= result.totalPages) {
          this.taskSearchValue.pageNumber = 0;
          this.searchTasks(this.taskSearchValue);
        }

        this.totalTasksFounded = result.totalElements;
        this.tasks = result.content; // format JSON
    });
  }

  paging(pageEvent: PageEvent) {
    if (this.taskSearchValue.pageSize !== pageEvent.pageSize) {
      this.taskSearchValue.pageNumber = 0;
    } else {
      this.taskSearchValue.pageNumber = pageEvent.pageIndex;
    }

    this.taskSearchValue.pageNumber = pageEvent.pageIndex;
    this.taskSearchValue.pageSize = pageEvent.pageSize;

    this.searchTasks(this.taskSearchValue);
  }

  toggleSearch(showSearch: boolean) {
    this.showSearch = showSearch;
  }

  updateCategoryCounter(category: Category) {
    this.categoryService.findeById(category.id).subscribe(cat => {
      this.categories[this.getCategoryIndex(category)] = cat;
      this.showCategoryDashboard(cat);
    });
  }

  private updateOverallCounter() {
    this.statService.getOverallStat().subscribe(result => {
       this.stat = result;
       this.uncompletedCountForCategoryAll = this.stat.uncompletedTotal;

       if (!this.selectedCategory) {
         this.fillDashData(this.stat.completedTotal, this.stat.uncompletedTotal);
       }
    });
  }

  getCategoryIndex(category: Category): number {
    const tmpCategory = this.categories.find(t => t.id === category.id);
    return this.categories.indexOf(tmpCategory);
  }

  private showCategoryDashboard(category: Category) {
    if (this.selectedCategory && this.selectedCategory.id === category.id) {
      this.fillDashData(category.completedCount, category.uncompletedCount);
    }
  }

  settingsChanged() {
    this.fillAllPriorities();
    this.searchTasks(this.taskSearchValue);
  }
}
