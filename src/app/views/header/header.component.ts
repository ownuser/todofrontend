import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {SettingsDialogComponent} from '../../dialog/settings-dialog/settings-dialog.component';
import {DeviceDetectorService} from 'ngx-device-detector';
import {DialogAction} from '../../object/DialogResult';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input()
  categoryName: string;

  @Output()
  toggleStat = new EventEmitter<boolean>();

  @Input()
  toggleStatistic: boolean;

  @Output()
  toggleMenu = new EventEmitter();

  @Output()
  settingsChanged = new EventEmitter();

  isMobile: boolean;
  isTablet: boolean;

  constructor(private dialog: MatDialog, deviceService: DeviceDetectorService) {
    this.isMobile = deviceService.isMobile();
    this.isTablet = deviceService.isTablet();
  }

  ngOnInit() {}

  toggleState() {
    this.toggleStat.emit(!this.toggleStatistic);
  }

  showSettings() {
    const dialogRef = this.dialog.open(SettingsDialogComponent, {
       autoFocus: false,
       width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.action === DialogAction.SETTINGS_CHANGED) {
          this.settingsChanged.emit(true);
          return;
      }
    });
  }

  onToggleMenu() {
    this.toggleMenu.emit();
  }
}
