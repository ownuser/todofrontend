import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Task} from 'src/app/model/Task';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {EditTaskDialogComponent} from '../../dialog/edit-task-dialog/edit-task-dialog.component';
import {ConfirmDialogComponent} from '../../dialog/confirm-dialog/confirm-dialog.component';
import {Priority} from '../../model/Priority';
import {Category} from '../../model/Category';
import {DeviceDetectorService} from 'ngx-device-detector';
import {TaskSearchValue} from '../../data/dao/search/SearchObjects';
import {DialogAction} from '../../object/DialogResult';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  public dataSource: MatTableDataSource<Task>;
  public displayedColumns: string[] = ['color', 'id', 'title', 'date', 'priority', 'category', 'operations', 'select'];

  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;

  tasks: Task[];
  categories: Category[];
  priorities: Priority[];

  selectedStatusFilter: boolean;
  taskSearchValue: TaskSearchValue;

  filterTitle: string;
  filterCompleted: number;
  filterPriorityId: number;
  filterSortColumn: string;
  filterSortDirection: string;
  readonly defaultSortColumn = 'title';
  readonly defaultSortDirection = 'asc';

  readonly iconNameDown = 'arrow_downward';
  readonly iconNameUp = 'arrow_upward';

  isMobile: boolean;
  isTablet: boolean;

  sortIconName: string;
  changed: boolean;
  completed: number;

  @Input()
  showSearch: boolean;

  @Input('tasks')
  private set setTasks(tasks: Task[]) {
    this.tasks = tasks;
    this.fillTable();
  }

  @Input('priorities')
  private set setPriority(priorities: Priority[]) {
    this.priorities = priorities;
    this.fillTable();
  }

  @Input('categories')
  private set setCategories(categories: Category[]) {
    this.categories = categories;
  }

  @Input('taskSearchValue')
  private set setTaskSearchValue(taskSearchValue: TaskSearchValue) {
    this.taskSearchValue = taskSearchValue;
    this.initSearchValues();
    this.initSortDirectionIcon();
  }

  @Input()
  selectedCategory: Category;

  @Input()
  totalTasksFounded: number;


  @Output()
  updateTask = new EventEmitter<Task>();

  @Output()
  deleteTask = new EventEmitter<Task>();

  @Output()
  selectCategory = new EventEmitter<Category>();

  @Output()
  filterByTitle = new EventEmitter<string>();

  @Output()
  filterByStatus = new EventEmitter<boolean>();

  @Output()
  filterByPriority = new EventEmitter<Priority>();

  @Output()
  addTask = new EventEmitter<Task>();

  @Output()
  paging = new EventEmitter<PageEvent>();

  @Output()
  toggleSearch = new EventEmitter<boolean>();

  @Output()
  searchAction = new EventEmitter();


  constructor(private dialog: MatDialog,
              private deviceService: DeviceDetectorService) {

                this.isMobile = deviceService.isMobile();
                this.isTablet = deviceService.isTablet();
               }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.fillTable();
  }

  private fillTable() {

    if (!this.dataSource) {
      return;
    }

    this.dataSource.data = this.tasks;

    this.dataSource.sortingDataAccessor = (task, colName) => {

      switch (colName) {
        case 'priority': {
            return task.priority ? task.priority.id : null;
        }
        case 'category': {
            return task.category ? task.category.title : null;
        }
        case 'date': {
            return task.date ? task.date.toString() : null;
        }
        case 'title': {
            return task.title;
        }
      }
    };
  }

  public getPriorityColor(task: Task): string{

    if (task.completed) {
      return '#F8F9FA';
    }

    if (task.priority && task.priority.color) {
      return task.priority.color;
    }

    return '#fff';
  }

  addNewTask() {
    const task = new Task(null, '', 0, null, this.selectedCategory);

    const dialogRef = this.dialog.open(EditTaskDialogComponent, {
      data: [task, 'Add Task', this.categories, this.priorities],
      width: '600px'
    });

    dialogRef.afterClosed().subscribe(result => {
        if (!result) {
          return;
        }

        if (result.action === DialogAction.SAVE) {
          this.addTask.emit(task);
        }
    });
  }

  editTaskDialog(task: Task) {

    const dialogRef = this.dialog.open(EditTaskDialogComponent, {
          data: [task, 'Edit Task', this.categories, this.priorities],
          width: '600px',
          autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {

        if (!result) {
          return;
        }

        if (result.action === DialogAction.DELETE) {
          this.deleteTask.emit(task);
          return;
        }

        if (result.action === DialogAction.COMPLETE) {
          task.completed = 1;
          this.updateTask.emit(task);
          return;
        }

        if (result.action === DialogAction.ACTIVE) {
          task.completed = 0;
          this.updateTask.emit(task);
          return;
        }

        if (result.action === DialogAction.SAVE) {
          this.updateTask.emit(task);
          return;
        }
    });
  }

  openDeleteDialog(task: Task) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '600px',
      data: {
        dataTitle: 'Confirm your action',
        message: `Are you sour want you wont delete this task: "${task.title}?"`
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {

      if (!result) {
        return;
      }

      if (result.action === DialogAction.OK) {
        this.deleteTask.emit(task);
        return;
      }

      if (result === DialogAction.CANCEL) {
        return;
      }
    });
  }

  onToggleStatus(task: Task) {
    task.completed = (task.completed === 0) ? 1 : 0;
    this.updateTask.emit(task);
  }

  onSelectCategory(category: any) {
    this.selectCategory.emit(category);
  }

  getMobilePriorityBgColor(task: Task): string {
    if (task.priority != null && !task.completed) {
      return task.priority.color;
    }

    return 'none';
  }

  pageChanged(pageEvent: PageEvent) {
    this.paging.emit(pageEvent);
  }

  onToggleSearch() {
    this.showSearch = !this.showSearch;
    this.toggleSearch.emit(this.showSearch);
  }

  initSortDirectionIcon() {
    this.sortIconName = (this.filterSortDirection === 'desc') ? this.iconNameDown : this.iconNameUp;
  }

  changeSortDirection() {
    this.filterSortDirection = (this.filterSortDirection === 'asc') ? 'desc' : 'asc';
    this.initSortDirectionIcon();
  }

  clearSearchValue() {
    this.filterTitle = '';
    this.filterCompleted = null;
    this.filterPriorityId = null;
    this.filterSortColumn = this.defaultSortColumn;
    this.filterSortDirection = this.defaultSortDirection;
  }

  checkFilterChanged(): boolean {

    this.changed = false;

    if (this.taskSearchValue.title !== this.filterTitle) {
      this.changed = true;
    }

    if (this.taskSearchValue.completed !== this.filterCompleted) {
      this.changed = true;
    }

    if (this.taskSearchValue.priorityId !== this.filterPriorityId) {
      this.changed = true;
    }

    if (this.taskSearchValue.sortColumn !== this.filterSortColumn) {
      this.changed = true;
    }

    if (this.taskSearchValue.sortDirection !== this.filterSortDirection) {
      this.changed = true;
    }

    return this.changed;
  }


  initSearch() {
    this.taskSearchValue.title = this.filterTitle;
    this.taskSearchValue.completed = this.filterCompleted;
    this.taskSearchValue.priorityId = this.filterPriorityId;
    this.taskSearchValue.sortColumn = this.filterSortColumn;
    this.taskSearchValue.sortDirection = this.filterSortDirection;

    this.searchAction.emit(this.taskSearchValue);

    this.changed = false;
  }

  private initSearchValues() {
    if (!this.taskSearchValue) {
      return;
    }

    this.filterTitle = this.taskSearchValue.title;
    this.filterCompleted = this.taskSearchValue.completed;
    this.filterPriorityId = this.taskSearchValue.priorityId;
    this.filterSortColumn = this.taskSearchValue.sortColumn;
    this.filterSortDirection = this.taskSearchValue.sortDirection;
  }
}
