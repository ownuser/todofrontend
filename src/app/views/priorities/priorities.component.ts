import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Priority} from '../../model/Priority';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmDialogComponent} from '../../dialog/confirm-dialog/confirm-dialog.component';
import {EditPriorityDialogComponent} from '../../dialog/edit-priority-dialog/edit-priority-dialog.component';
import {DialogAction} from '../../object/DialogResult';

@Component({
  selector: 'app-priorities',
  templateUrl: './priorities.component.html',
  styleUrls: ['./priorities.component.css']
})
export class PrioritiesComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  static defaultColor = '#fff';


  @Input()
  priorities: [Priority];

  @Output()
  addPriority = new EventEmitter<Priority>();

  @Output()
  updatePriority = new EventEmitter<Priority>();

  @Output()
  deletePriority = new EventEmitter<Priority>();


  ngOnInit() {
  }

  onAddPriority() {

    const dialogRef = this.dialog.open(EditPriorityDialogComponent, {
        data: [new Priority(null, '', PrioritiesComponent.defaultColor), 'Add new Priority'],
        width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {

      if (!result) {
        return;
      }

      if (result.action === DialogAction.SAVE) {
        const newPriority = new Priority(null, result.obj.title as string, PrioritiesComponent.defaultColor);
        this.addPriority.emit(newPriority);
      }
    });
  }

  onDeletePriority(priority: Priority) {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dataTitle: 'Confirm your action',
        message: `Are you sour want you wont delete this priority: "${priority.title}?"`
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {

      if (!result) {
        return;
      }

      if (result.action === DialogAction.OK) {
        this.deletePriority.emit(priority);
      }
    });
  }

  onUpdatePriority(priority: Priority) {

    const dialogRef = this.dialog.open(EditPriorityDialogComponent, {

      data: [new Priority(priority.id, priority.title, priority.color), 'Edit Priority'],
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {

      if (!result) {
        return;
      }

      if (result.action === DialogAction.DELETE) {
        this.deletePriority.emit(priority);
        return;
      }

      if (result.action === DialogAction.SAVE) {
        priority = result.obj as Priority;
        this.updatePriority.emit(priority);
        return;
      }
    });
  }
}
