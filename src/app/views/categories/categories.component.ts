import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Category} from '../../model/Category';
import {MatDialog} from '@angular/material/dialog';
import {EditCategoryDialogComponent} from '../../dialog/edit-category-dialog/edit-category-dialog.component';
import {DeviceDetectorService} from 'ngx-device-detector';
import {CategorySearchValue} from '../../data/dao/search/SearchObjects';
import {DialogAction} from '../../object/DialogResult';


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categories: Category[];
  selectedCategory: Category;
  isMobile: boolean;
  isTablet: boolean;
  indexMouseMove: number;
  searchCategoryTitle: string;
  filterChanged: boolean;
  showEditIconCategory: boolean;
  categorySearchValue: CategorySearchValue;
  uncompletedCountForCategoryAll: number;

  constructor(private dialog: MatDialog,
              private diviceServise: DeviceDetectorService) {

                this.isMobile = this.diviceServise.isMobile();
                this.isTablet = this.diviceServise.isTablet();
              }

  @Input('selectedCategory')
  set setCategory(selectedCategory: Category) {
    this.selectedCategory = selectedCategory;
  }

  @Input('uncompletedCountForCategoryAll')
  set uncompletedCount(uncompletedCountForCategoryAll: number) {
    this.uncompletedCountForCategoryAll = uncompletedCountForCategoryAll;
  }

  @Input('categories')
  set setCategories(categories: Category[]) {
    this.categories = categories;
  }

  @Input('categorySearchValue')
  set setCategorySearchValue(categorySearchValue: CategorySearchValue) {
    this.categorySearchValue = categorySearchValue;
  }


  @Output()
  selectCategory = new EventEmitter<Category>();

  @Output()
  updateCategory = new EventEmitter<Category>();

  @Output()
  deleteCategory = new EventEmitter<Category>();

  @Output()
  addCategory = new EventEmitter<Category>();

  @Output()
  searchCategory = new EventEmitter<CategorySearchValue>();

  ngOnInit(): void {}

  showTaskByCategory(category: Category): void{

    if (this.selectedCategory === category) {
      return;
    }
    this.selectedCategory = category;
    this.selectCategory.emit(this.selectedCategory);
  }

  showEditIcon(index: number) {
    this.indexMouseMove = index;
  }

  openEditDialog(category: Category) {
    const dialogRef = this.dialog.open(EditCategoryDialogComponent, {
       data: [new Category(category.id, category.title, category.completedCount, category.uncompletedCount), 'Edit Category'],
       width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {

      if (!result) {
        return;
      }

      if (result.action === DialogAction.DELETE) {
        this.deleteCategory.emit(category);
        return;
      }

      if (result.action === DialogAction.SAVE) {
        this.updateCategory.emit(result.obj as Category);
        return;
      }
    });
  }

  addNewCategory() {
    const dialogRef = this.dialog.open(EditCategoryDialogComponent, {
        data: [new Category(null, ''), 'Add Category'],
        width: '400px'
     });

    dialogRef.afterClosed().subscribe(result => {
         if (!result) {
           return;
         }

         if (result.action === DialogAction.SAVE){
           this.addCategory.emit(result.obj as Category);
         }
     });
  }

  search() {

    this.filterChanged = false;

    if (!this.categorySearchValue) {
      return;
    }
    this.categorySearchValue.title = this.searchCategoryTitle;
    this.searchCategory.emit(this.categorySearchValue);
  }

  checkFilterChange() {

    this.filterChanged = false;

    if (this.searchCategoryTitle !== this.categorySearchValue.title) {
      this.filterChanged = true;
    }

    return this.filterChanged;
  }

  clearAndSearch() {
    this.searchCategoryTitle = null;
    this.search();
  }
}
