export class DialogResult {
  action: DialogAction;
  obj: any;

  constructor(action: DialogAction, obj?: any) {
    this.action = action;
    this.obj = obj;
  }
}

export enum DialogAction {
  SETTINGS = 0, // settings was changed
  SAVE = 1, // save changed
  OK = 2, // for confirm change
  CANCEL = 3, // cancel all thins
  DELETE = 4, // delete object
  COMPLETE = 5, // complete task
  ACTIVE= 6, // rollback task in active
  SETTINGS_CHANGED = 7 // change priority settings color
}
